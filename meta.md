ShellTux
========

TODOs Oktober 2018
------------------
* shelltux start / next
    * if there is more than one option in the Aufgaben Tree, then show options for what to do next
* Plasma:
    * Eigenes programm via desktop starten
    * Weiche zu Gnome-Desktop
* Bash: $ type
* shelltux start --reset  --> setzt die aktuelle Aufgabe auf ungelöst und startet sie neu
* FIX: siehe solution-later

TODOs 2019
----------
Verschiedene gesammelte TODOs:

* Aufgaben-Ideen:
    * Programm gibt Sterne aus
        * Zähle Sterne
        * Ändere das Programm so, dass es doppelt so viele Sterne ausgibt
    * Töne:
        * Höre dir die Tonfolge an und merke dir die Tonhöhe
        * Welche Antwort ist richtig? -> "höher, höher, tiefer, höher"

    * while & convert image
        * png -> png -> png
        * jpg -> jpg -> jpg
        * Unterschied?





Colors
------
siehe a1.17.2-echo-colors, Ergänzungen:

Alles über Farben: https://misc.flogisoft.com/bash/tip_colors_and_formatting --> copy?

todo: green background: https://unix.stackexchange.com/questions/94498/what-causes-this-green-background-in-ls-output

Farben auf der Bash:
    $ cowsay Hallo | sed "s/Hallo/$(echo -en "\033[32m")\0$(echo -e "\033[0m")/g"

---> Idee: das Planetenprogramm hat einen vorgefertigten Text, der Zeile für Zeile mit Delay ausgegeben wird

Idee: Tannenbaum mit executable files
    erinnere dich an den Baum in a1.12.1-touch: Färbe den oberen Teil grün, indem du die Dateien ausführbar machst

sudo gem install lolcat
lolcat.ruby2.4 -a -s 40 a

cowsay < a

interessant sed-Nutzung:
- https://www.unix.com/unix-for-dummies-questions-and-answers/134824-using-sed-change-specific-words-color.html
- https://stackoverflow.com/questions/28282480/sed-match-regex-in-specific-position

Fiese Dateinamen
----------------
* von Simon

Unsortiert
----------
### Story
- Ausbildung zum Bash-Detektiv
- "Tipp" (ohne Meister o. ä.)
- siehe https://lifehacker.com/5633909/who-needs-a-mouse-learn-to-use-the-command-line-for-almost-anything
    - cd path/1/2
    - cd -
    - create and remove folders / files

### Linux-Buch auf Deutsch
* https://kofler.info/buecher/linux/

### den Figlet-"title" in eine extra Datei?
- jede Übung sollte eine Datei "title" bekommen DONE, damit man diese in der Statusübersicht anzeigen kann TODO

### DONE: Kapitel 1 a
Aufgabe von Mr. X: "Wieviele Dateien enthält das Verzeichnis aaa? Wieviele versteckte Dateien?"
--> DONE: a1.5-ls-hidden

### wikitolearn
* MOVE to here: https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/FAQ/Die_Kommandozeile
    * ...
    * Strg+R
    * history
    * Tastenkürzel
    * Yakuake
    * xdg-open
    * cool-retro-term
    * nms, no-more-secrets
    * cowsay Hallo | nms
    * geht das noch?? https://engineering.pinterest.com/blog/hack-pinterest

### Siehe Befehlsreferenz
* ...

### Planeten erkunden / Astronomie
todo

### später tail, watch, du -hc
- ...

### zwischendurch:
- Kombinationsaufgaben?
    - Aber das sollte sich durch neue Aufgaben von selbst ergeben,
    - z. B. nach "a1.21-own-script" -> for-Schleife zum Umbenennen von Dateien
        * zurückgestellt: "alter mann kam mit der Zeitmaschine aus der Vergangenheit oder einfach es kommen Bilder an und die m[ssen yusammenkopiert werden"

### Tilde / HOME
- Umgebungsvariablen
    - printenv
    - echo $HOME
        - Tilde, ~, My Home is my Castle
    - $PATH, which etc.
    - cd ohne was geht ins HOME

### .bashrc - Eigene Begrüßung mit Cowsay
- z. B. echo Hallo
- oder cowsay Hallo $user
- oder figlet Willkommen
- oder echo Die Zeit ist $(date)

### File Size / Dateigröße
* Dateigrößen erfahren mit Kate und Text-Artikeln (1 Byte, 10, 100, 1024, etc.)
* oder ...Planetenabstände...

### mit ssh auf einen anderen Rechner verbinden
- was tun?

### Prompt verstecken, pwd
    $ function __prompt_command() { PS1="" ; }
    $ function __prompt_command() { PS1="Mr.X :-( " ; }

Gut zu wissen: wie kann man sich eine Funktionsdefinition anschauen?
https://stackoverflow.com/questions/6916856/can-bash-show-a-functions-definition
    $ type __prompt_command
    $ set # zeigt alle Funktionen an!

### sleep, loop
* irgendwas ausgeben und immer schneller werden

### Struktur
* Unteraufgaben ggf. in einen Unterordner unter aufgaben/
    * dann als Subgraph mit dot anzeigen

### bc: Zahlen in einer Datei multipliziern und addieren
* ...
* Achtung bei Floating point
    $ echo "$a * 0.5" | bc -l
    .05
    https://www.shell-tips.com/2010/06/14/performing-math-calculation-in-bash/
* 5 mal 10 plus 11 (aber was ist mit punkt vor strich?)
* also lieber nur 5 mal 10

### Alle Befehle in allen PFADEN auflisten

    $ for a in $(echo $PATH | tr ":" "\n") ; do if [ -e $a ] ; then ls -1 $a ; fi ; done
        | wc -l   # 4811
        # dort sind auch interessante Farben zu sehen

    $ for a in $(echo $PATH | tr ":" "\n") ; do if [ -e $a ] ; then find $a -type f -executable ; fi ; done
        | wc -l   # 4105

    # nur basename / mit -exec (dauert viel länger):
    $ for a in $(echo $PATH | tr ":" "\n") ; do if [ -e $a ] ; then find $a -type f -executable -exec basename {} \; ; fi ; done
        | wc -l   # 4105

    # nur basename / mit GNU -printf (dauert viel länger):
    # siehe man find: "%f     File's name with any leading directories removed (only the last element)."
    $ for a in $(echo $PATH | tr ":" "\n") ; do if [ -e $a ] ; then find $a -type f -executable -printf "%f\n" ; fi ; done
        | wc -l   # 4105

Da fehlen meistens die Symbolic links auf executable. Siehe auch readlink.

### Die 10 zuletzt geänderten Dateien auflisten
* mit find

### HTML-Tabelle erstellen mit den Zahlen
* und dann auf den eigenen Rechner zum Anschauen übertragen

### Check Last Time User Logged In On The System
- https://www.cyberciti.biz/faq/unix-linux-check-last-time-user-loggedin-command/
    - last | less

### Split-Window-Challenge
* Eine Rakete muss landen (Fenster 1) (mit ncurses, siehe alte Arbeitsblätter?)
* Gesteuert über eine Kontroll-Datei (Schubkraft und Dauer) (Fenster 2)
* Ausgabe in einer weiteren Datei (Fenster 3)
* siehe auch https://www.xarg.org/puzzle/codingame/mars-lander/

### Reguläre Ausdrücke
* deutsch
    * mit Python: https://de.wikibooks.org/wiki/Python_unter_Linux:_Regul%C3%A4re_Ausdr%C3%BCcke
    * mit grep: http://kushellig.de/linux-unix-grep-befehl-beispiele/
    * viel Infos, aber Copyright: http://openbook.rheinwerk-verlag.de/linux/linux_kap08_001.html
    * weiteres: https://www.python-kurs.eu/re.php

### Kapitel 1 b
- "Um die Zeilen nicht einzeln zu zählen, verwende wc -l; z. B. cat datei | wc -l"

Aufgabe von Mr. X: "Zähle alle Wörter in der Datei x"

Wer ist Mr. X? --> Indiana Goof? Der Pinguin Tux?

$ cowsay -f tux Glückwunsch. Du hast mich gefunden. Ich bin Tux der Pinguin.

### NEXT: Variablen, mit und ohne export
* kleines Script schreiben zum Vertauschen

### Status: Graph-View und Dependencies
* "Diese Aufgabe erfordert Vorwissen aus Aufgabe xyz
    Dorthin springen?
    Dennoch fortfahren?"
* "Für diese Aufgabe muss folgendes Paket installiert sein: ... . Abbruch"

### grep mit wc -l

Aufgabe von Mr. X: "Ich habe meinen Namen in einer Datei versteckt"
--> Lerne cat Datei | grep "mr. x = "
(vorher Datei mit vielen Zeilen anlegen; vorher per cat manuell nach etwas suchen lassen)

- Liste mit Mappings Geheimcode -> Kapitel

### grep und Python-Zufallszahlen
* [grep und Python-Zufallszahlen](http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/grep)

### sed
* siehe src-arbeitsblatt-pub/arbeitsblatt-sed.adoc
* siehe src-arbeitsblatt-pub/arbeitsblatt-sed.meta

### todo: ... cat DATEI | grep TEXT ...
* anstelle von cat DATEI | grep TEXT
    * lieber grep TEXT DATEI
    * oder grep TEXT dok*        !!!! kann in eine gute Aufgabe verwandelt werden

### grep mit wc -l
    $ grep Hallo | wc -l

### wc -l mit Variablen
    - $ echo "Die Anzahl ist $(cat a | wc -l)."
    - Für besonders große Dateien, damit selbst less zu langsam ist

### Ctrl+C, Strg+C
- ...

### bc
- Rechne große Zahlen aus, die Mr. X als Aufgabe stellt
- echo "5 * 6" | bc

### Weiteres 1 / Später
- Dateien bearbeiten mit dem nano-Editor
        - "Eine Datei bearbeiten/korrigieren. In die Rechnung hat sich ein Fehler eingeschlichen.
            dieser muss korrigiert werden"
        - "Eine Datei selber erstellen." (oder lieber mit touch und echo?)
- compile a C++ program
- chmod -x
- chmod g-w
- youtube-dl
    - Mögliche Beispielvideos:
        - ["Was hat mein Essen mit dem Klima zu tun"](https://www.youtube.com/watch?v=COiCdy9opLw), 3 min, 2013, vom Bundeszentrum für Ernährung
        - ["Neue Düngeverordnung: Weniger Nitrat im Trinkwasser | Unser Land | BR"](https://www.youtube.com/watch?v=l6S7z_Ujf4M), Doku-Ausschnitt, 6 min
        - ["Die unglaubliche Geschichte eines Löffels"](https://www.youtube.com/watch?v=ebuRHcv7TtI), Greenpeace 2015, 2 min
        - ["Gier nach Pelz: So leiden die Tiere in Europa"](https://www.youtube.com/watch?v=Y3_7Kqo3_Dg), 2017, 13 min, ARD Plusminus
- screenshots mit maim (geht nur mit X)
- bg, fg, Strg+Z (print AAABBBA ?)
    - siehe anleitungen/bash-background-jobs-mit-oneko/README.md

### Prozesse auch mit Plasma System Monitor finden und beenden
...

### Moon-Buggy selber programmieren
...

### Moon-Lander mit ncurses?
* ...
* todo: ["Getting Started with ncurses"](http://www.linuxjournal.com/content/getting-started-ncurses), 2018
    - Beispiel: Sierpinski's Triangle
    - from http://opensource-usability.blogspot.de/2018/01/programming-with-ncurses.html
* TODO: move to here: /home/gregor/dataDocuments/FS_Computer-AG/src-arbeitsblatt-pub/arbeitsblatt-py-curses.adoc
* TODO: move to here: /home/gregor/dataDocuments/FS_Computer-AG/src-arbeitsblatt-pub/arbeitsblatt-py-casino.adoc / Roulette
* TODO: move to here: /home/gregor/dataDocuments/FS_Computer-AG/src-arbeitsblatt-pub/arbeitsblatt-py-casino.meta
* ...

### Weiteres 2 / Später
- ls -F - falls man Verzeichnisse nicht sieht
    - l
    - ll
    - etc.
- Talking with Eliza
- cat -n für Zeilennummern
- grep -n für Zeilennummern
- PS1='' und nun finde dich mit pwd zurecht... (todo: PS1 lässt sich in der Session nicht so einfach setzen)
- $ stress
    - https://stackoverflow.com/questions/2925606/how-to-create-a-cpu-spike-with-a-bash-command
- espeak

### dict / Wörterbuch, todo
Liste von deutschen Wörtern:
    $ iconv -f ISO-8859-15 -t utf8 /usr/share/myspell/de_DE.dic | grep färben | wc -l
    (https://askubuntu.com/questions/627572/german-words-in-ubuntu-usr-share-dict)
    Englische Wörter: $ cat /usr/share/myspell/en_US.dic | sed -e s#/.*## | grep -Ei "linux|tree"

todo: tolle Liste von Dingen, die man verwenden kann: https://learn.adafruit.com/an-illustrated-guide-to-shell-magic-typing-less-and-doing-more?view=all

* Offline-Dict
    * ding geht, aber nur mit GUI
    * $ grep -E ^Baum -h /usr/share/dict/de-en.txt
    * $ grep -Ei ^linux -h /usr/share/dict/de-en.txt
    * https://wiki.ubuntuusers.de/W%C3%B6rterb%C3%BCcher/
        * todo: dictd funktioniert nicht auf opensuse, warum?
        * siehe hier? http://wiki.nikhil.io/Dictd_and_Dictionaries_on_Linux

### watch: Zeige eine Uhr mit figlet an
$ watch -n 1 'echo -n "Uhr: " > a ; date +%H:%M:%S >> a ; cat a | figlet'

### Enter drücken und Geschwindigkeit messen / Takt für Musik
see also codestruct-util: tool-game-enter-rhythm
$ while true ; do a=$(/usr/bin/time --format=%e bash -c "read" 2>&1 1>/dev/null); echo -n "$a "; b=$(echo "1/$a*10" | bc) ; for ((n=0;n<$b;n++)); do echo -n "#" ; done; done

for loop: https://stackoverflow.com/questions/3737740/is-there-a-better-way-to-run-a-command-n-times-in-bash
redirect time output: https://unix.stackexchange.com/questions/12068/how-to-measure-time-of-program-execution-and-store-that-inside-a-variable

### Spiegelkuh (nicht so toll)
    $ cowsay $(echo Hallo | rev) | rev
        geht nicht so auf Anhieb, weil die Spaces fehlen!
        siehe auch /home/gregor/tmp/test.sh (https://stackoverflow.com/questions/6980090/how-to-read-from-a-file-or-stdin-in-bash)
            das geht auch nicht, wegen den Spaces
        https://unix.stackexchange.com/questions/354092/bash-add-trailing-spaces-to-justify-string
            printf '%-40sa' Test
            -> Test                       a

    $ cowsay Hallo | tac
        sieht schon besser aus

        cowsay $(echo Hallo | rev) | ./test2.py | rev
                                          _______
                                         > Hallo <
                                          -------
                                  ^__^   \
                          _______\)oo(  \
                      \/\)       \)__(
                         | w----||
                         ||     ||

            #!/usr/bin/python3

            import fileinput
            # https://stackoverflow.com/questions/1450393/how-do-you-read-from-stdin-in-python

            for line in fileinput.input():
                if line.endswith('\n'):
                    line = line[:-1]
                #print(line)
                # https://pyformat.info/#string_pad_align
                print("{0:50}".format(line))

### Das Merkblatt
todo: Optionales Wissen mit Sternchen versehen?

### Andere Tutorials und Websuche
- "ShellTux" gibt es noch nicht

### Bash-Scripting-Tutorial und andere gute Tutorials!
- Übersicht über Operatoren: https://ryanstutorials.net/bash-scripting-tutorial/bash-if-statements.php
    - SEHR GUT: https://ryanstutorials.net/bash-scripting-tutorial
    - Programming Challenges (Programmiersprachen-agnostisch): https://ryanstutorials.net/programming-challenges/

#### Deutsch
- https://github.com/ronreiter/interactive-tutorials
    - https://www.learnshell.org/de
- http://www.linux-services.org/shell/
- http://www.selflinux.org/selflinux/pdf/shellprogrammierung.pdf
- https://wiki.ubuntuusers.de/Shell/Bash-Skripting-Guide_f%C3%BCr_Anf%C3%A4nger/

#### Englisch
- https://lifehacker.com/5633909/who-needs-a-mouse-learn-to-use-the-command-line-for-almost-anything
    - cd -
    - nano
    - ls -l | less
    - rename - batch renaming
    - DONE: rm *
    - DONE: ps aux
    - ls -lSr
        - S by size
        - r reverse
- http://www.tuxarena.com/intro/intro.php
    - ausführliches Cheat-Sheet: http://www.tuxarena.com/intro/cheatsheet.html
    - id
    - whatis
    - cal
    - file
- Math in bash:
    - https://www.shell-tips.com/2010/06/14/performing-math-calculation-in-bash/
    - https://superuser.com/questions/256023/can-i-do-basic-maths-in-bash
- grep: http://kushellig.de/linux-unix-grep-befehl-beispiele/#fazit

### Aufpassen
* https://phabricator.kde.org/D11859 - Copy&paste exploits

Warum bash?
-----------
- Bash ist auf Platz 7 der populärsten Projekte dieser Seite: https://www.openhub.net
- https://www.openhub.net/p/bash

DONE
----
### Storyline - Einleitung
Mr. X war auf dem Computer und hat Spuren hinterlassen.

Deiner Aufgabe ist es herauszufinden, wer Mr. X ist.

Auf dem Weg dorthin wirst du mit der Linux-Shell Bash arbeiten
und schrittweise kennenlernen wie diese funktioniert.

Auf dem Weg musst du dir Notizen auf ein Merkblatt machen, das
du selber erstellst. Entweder aus Papier oder in einer Datei.
