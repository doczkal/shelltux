ShellTux - Das interaktive Bash-Tutorium - DEV
==============================================

Fehler gefunden?
----------------
* Bitte auf der [Issues-Seite melden](https://framagit.org/feinstaub/shelltux/issues).

TODO
----
- siehe meta.md
- Cleanup .cache (bisher nur vor jeder Aufgabe mit shelltux start oder next)
    - Wenn eine Aufgabe bereits gelöst wurde, aber man eine Aufgabe mit shelltux start xxx nochmal gestartet hat,
        und man dann nach erfolgreicher Lösung anstelle shelltux start/next? gleich wieder shelltux solve aufruft,
        dann ist die Aufgabe erfolgreich gelöst
- Translate

Funktionen
----------
### source shelltux init
- Erzeugt ein Alias auf den absoluten Pfad von shelltux.
- Trägt das Alias in die .bashrc ein

### source shelltux start [AUFGABE]
- Das Source ist schon im Alias enthalten.
- Startet die aktive Aufgabe neu.
- Optional: eine Aufgabe direkt starten

### source shelltux solve
- Das Source ist schon im Alias enthalten.
- Prüft, ob die die Aufgabe gelöst wurde oder ruft die Eingabeaufforderung zur Eingabe der Lösung auf.

Hilfsmodule
-----------
### lib/stutil.py --first-aufgabe
- Gibt den Namen der ersten Aufgabe aus

### lib/stutil.py --nextnew-aufgabe
- Gibt den Namen der nächsten neuen Aufgabe zurück.

### lib/stutil.py --aktive-aufgabe
- Gibt den Namen der aktiven Aufgabe zurück oder nichts, wenn nichts aktiv.

### lib/stutil.py --set-aktive-aufgabe AUFGABE
- Setzt die aktive Aufgabe.

### lib/stutil.py --aktive-aufgabe-set-solved
- Markierte die aktuelle Aufgabe als gelöst

### lib/stutil.py --aktive-aufgabe-query-solution
- Fragt nach der Lösung der aktuellen Aufgabe und markiert sie als gelöst, wenn richtig.

Konzept
-------

### Aufgaben
- Unter ./aufgaben/ liegen die einzelnen Aufgaben als Verzeichnisse.
- Die Sortierung ergibt die Reihenfolge der Aufgaben.
- Die Reihenfolge ist mit Dolphin oder ls -v ("natural sort of (version) numbers within text") zu bestimmen.

### Status
- In der Datei ~/.config/shelltux/status wird der Status jeder Aufgabe hinterlegt:
z. B.:
a1.1-intro:     solved
a1.2-ls:        solved
a1.3-ls-cd:     new
a1.4-ls-cd:     new
a1.5-ls-cd:     new
a1.10:          new
Das ganze im JSON-Format
wobei gilt:
    - new:    neu und/oder noch nicht bearbeitet
    - solved: gelöst

### Aktive Aufgabe
- Es gibt genau eine aktive Aufgabe.
- Der Name ist hier gespeichert: ~/.config/shelltux/activetask

### Arbeitsverzeichnis
- Das Verzeichnis ~/.cache/shelltux/ ist das Arbeitsverzeichnis.
- Hier legen die Aufgaben ihre Dateien ab.
