Chat-Korpara
============

* Dortmunder-Chat-Korpus: http://www.chatkorpus.tu-dortmund.de/staccado.html
* Download: http://www.chatkorpus.tu-dortmund.de/korpora.html
    * "[...] haben wir sechs Kompilationen erzeugt, die jeweils unterschiedlich zugeschnittene Ausschnitte aus dem Gesamtkorpus umfassen und die wir auf dieser Seite zum Download und zur freien Nutzung bereitstellen."
* "Das Releasekorpus als Online-Version (HTML) und als abfragbare Download-Version (XML)"
    * **Chat-Korpora/releasekorpus+staccado.zip** (3,7 MB) --> **Chat-Korpora/Releasekorpus**
    * Interaktives Such-Interface: `java -jar STACCADo\ 1.0.jar`
        * z. B. Suche nach "regn" ergibt u. a.
            * "Chat-Korpora/Releasekorpus/Chat Korpus/1000000 Professionelle Chats/1300000 Medienkontext/1306000 Bluewin/1306016_bluewin_Thomas_Bucheli_28-10.xml"

* Rausgepickt:
    * Studienberatung: 1202016_RUB_Beratung_nachSystemcrash_17-06-2004.xml, Jahr 2014
    * Meterologe: 1306016_bluewin_Thomas_Bucheli_28-10.xml, Jahr 2000
