import stutil
import os.path

"""
INSTALL:
    sudo pip install -U pytest
    # see https://docs.pytest.org/en/latest/index.html

RUN:
    pytest
    pytest -s # to disable capturing output; for debugging
"""

def test_natural_sorted():
    l = stutil.natural_sorted([ "1.3", "1.20", "1.2" ])
    assert l == [ "1.2", "1.3", "1.20" ]

def mock_get_aufgaben_location():
    """
            a
            |
            b
            |
            c
            |
            d
          /   \
         e     f
             / | \
          fc1 fc2 fc3
        /  |   |   |  \
       g   h   k   m   n
    """
    return os.path.realpath('testdata/aufgaben')

def mock_get_config_location():
    return os.path.realpath('testdata/.config')

def mock_get_work_dir(aufgabe):
    return os.path.realpath(os.path.join('testdata/.cache/shelltux', aufgabe))

stutil.get_aufgaben_location = mock_get_aufgaben_location
stutil.get_config_location = mock_get_config_location
stutil.get_work_dir = mock_get_work_dir

# TODO: remove testdata/.config !!!

stutil.init_config_and_cache() # !!!

def test_get_direct_children():
    c = stutil.get_direct_children('d')
    assert c == ['e', 'f']

    c = stutil.get_direct_children('a')
    assert c == ['b']

    c = stutil.get_direct_children('e')
    assert c == []

def test_get_parent_with_atleast_2_children():
    a = stutil.get_parent_with_atleast_2_children('e')
    assert a == 'd'

def test_get_next_aufgaben():
    aufgaben = stutil.get_available_aufgaben()
    for a in aufgaben:
        print(a + ' --> ' + str(stutil.get_next_aufgaben(a)))

    assert stutil.get_next_aufgaben(None) == [ 'a' ]
    assert stutil.get_next_aufgaben('a') == [ 'b' ]
    assert stutil.get_next_aufgaben('b') == [ 'c' ]
    assert stutil.get_next_aufgaben('c') == [ 'd' ]
    assert stutil.get_next_aufgaben('d') == [ 'e', 'f' ]
    assert stutil.get_next_aufgaben('e') == [ 'e', 'f' ]
    assert stutil.get_next_aufgaben('f') == [ 'fc1', 'fc2', 'fc3' ]
    assert stutil.get_next_aufgaben('fc1') == [ 'g', 'h' ]
    assert stutil.get_next_aufgaben('fc2') == [ 'k' ]
    assert stutil.get_next_aufgaben('fc3') == [ 'm', 'n' ]

def test_get_next_aufgabe_interactive():
    aufgaben = stutil.get_available_aufgaben()
    for a in aufgaben:
        print("-----")
        print("Aktive Aufgabe ist: " + a)
        stutil.set_aktive_aufgabe(a)
        ### a2 = stutil.get_next_aufgabe_interactive() # uncomment to test; use with $ pytest -s
        ### print("Result: " + str(a2))
