#!/usr/bin/python3

#
# needs Python >= 3.5 for allow_abbrev
# commented out, so needs 3.4
#

import argparse
import os
from pathlib import Path
import re
import json
import subprocess

cache_aufg_nr_map = { }

def natural_sorted(l):
    """
    from https://stackoverflow.com/questions/4836710/does-python-have-a-built-in-function-for-string-natural-sort
    """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)

def get_lib_dir():
    libdir = os.path.dirname(os.path.realpath(__file__))
    return libdir

def get_aufgaben_location():
    libdir = get_lib_dir()
    return os.path.realpath(os.path.join(libdir, '..', 'aufgaben'))

def my_read_text(p):
    """
    TODO: remove when possible
    """
    with open(p.as_posix(), 'r') as f:
        return f.read()

def my_write_text(p, data):
    """
    mimic Path.write_text, because this is only available for Py 3.5 / TODO remove
    Open the file in text mode, write to it, and close the file.
    """
    #  otherwise: TypeError: invalid file: PosixPath('/home/maxm/.config/shelltux/activetask')
    with open(p.as_posix(), 'w') as f:
        f.write(data)

def get_aufgabe_dir(aufgabe):
    """
    The source dir for this aufgabe: $ST_CURRENTAUFGABEDIR
    """
    return os.path.join(get_aufgaben_location(), aufgabe)

def get_solution_fixed(aufgabe):
    """
    Returns einen String, der als Lösung eingegeben werden muss
    None, wenn keine fixe Lösung vorgegeben
    """
    solutionfile = os.path.join(get_aufgabe_dir(aufgabe), 'solution')
    if os.path.exists(solutionfile):
        # contents = Path(solutionfile).read_text() # orig
        contents = my_read_text(Path(solutionfile))
        return contents
    return None

def get_solution_simple_ack(aufgabe):
    """
    Returns eine Frage, die bestätigt werden muss (simple acknowlegement)
    None, wenn keine Lösung dieser Art vorgegeben ist
    """
    solutionfile = os.path.join(get_aufgabe_dir(aufgabe), 'solution-simple-ack')
    if os.path.exists(solutionfile):
        # contents = Path(solutionfile).read_text() # orig
        contents = my_read_text(Path(solutionfile))
        return contents
    return None

def get_solution_later(aufgabe):
    """
    Returns True, wenn für die Aufgabe zwar eine automatische Lösung möglich wäre, bisher aber keine implementiert wurde
    Ansonsten False
    """
    solutionfile = os.path.join(get_aufgabe_dir(aufgabe), 'solution-later')
    return os.path.exists(solutionfile)

def get_solution_test_script(aufgabe):
    """
    Returns den Pfad zur solution-test-Datei oder None, wenn keine vorhanden
    """
    solutiontestfile = os.path.join(get_aufgabe_dir(aufgabe), 'solution-test')
    if os.path.exists(solutiontestfile):
        return solutiontestfile
    return None

def get_work_dir(aufgabe):
    """
    The user working dir for this aufgabe: $ST_CURRENTWORKDIR
    """
    return os.path.join(os.path.expandvars('$HOME/.cache/shelltux'), aufgabe)

def run_solution_test(aufgabe):
    """
    Returns True (exit code 0) oder False (exit code not 0)
    """
    solutiontestfile = get_solution_test_script(aufgabe)
    print(solutiontestfile)
    # Python 3.6.x # cp = subprocess.run([ solutiontestfile, get_work_dir(aufgabe) ])
    # Python 3.6.x # return cp.returncode == 0;
    # Python 3.4: (TODO: switch to Python 3.6 when possible)
    rc = subprocess.call([ solutiontestfile, get_work_dir(aufgabe) ])
    return rc == 0

def get_config_location():
    return os.path.expandvars('$HOME/.config/shelltux')

def get_status_file_location():
    return os.path.join(get_config_location(), 'status')

def read_status_file():
    """
    returns a dict: aufgabe name --> status (new, active, solved)
    """
    statusf = get_status_file_location()
    if os.path.exists(statusf):
        with open(statusf) as f:
            d = json.load(f)
            return d
    else:
        return {}

def write_status_file(d):
    """
    writes the status file with the given dict
    """
    statusf = get_status_file_location()

    parentdir = os.path.dirname(statusf)
    if not os.path.exists(parentdir):
        Path(parentdir).mkdir(parents=True)

    with open(statusf, 'w') as f:
        json.dump(d, f)

def get_available_aufgaben():
    # print(get_aufgaben_location())

    p = Path(get_aufgaben_location())
    dirs = [x.name for x in p.iterdir() if x.is_dir()]
    #print(dirs)
    aufgaben_from_dir = natural_sorted(dirs)
    #print(aufgaben_from_dir)
    return aufgaben_from_dir

def create_update_status():
    """
    Aus den vorhandenen Aufgaben-Verzeichnissen und dem bisherigen
    gespeicherten Status (gelöst oder nicht) wird der neue Status
    gespeichert, wo ggf. neue Aufgaben (als new) integriert werden.
    """
    aufgaben_from_dir = get_available_aufgaben()

    # print(get_status_file_location())
    fd = read_status_file() # file dict

    md = {} # merged dict

    for k in aufgaben_from_dir:
        if k in fd:
            md[k] = fd[k]
        else:
            md[k] = 'new'

    # print(md)
    write_status_file(md)

def init_config_and_cache():
    """
    Run once for a session
    """

    # Always look for new Aufgaben in the Aufgaben directory
    # (This call wastes resources if there was no change in the Aufgaben directory
    #  so this can be optimized later)
    create_update_status()

    # Needed when an Aufgaben folder is added, removed or renamed
    refresh_cache_aufg_nr_map()

def get_aufgabe_title(aufgabe):
    """
    Reads the string in the 'title' file and returns it
    """
    titlefile = os.path.join(get_aufgabe_dir(aufgabe), 'title')
    if os.path.exists(titlefile):
        return my_read_text(Path(titlefile)).strip()
    else:
        return "Kein Titel"

def get_aktive_aufgabe():
    """
    Name der aktiven Aufgabe oder None.
    None, falls noch keine aktive Aufgabe oder aktive Aufgabe nicht existiert
    """
    p = Path(os.path.join(get_config_location(), 'activetask'))
    aufgabe = None
    if p.exists():
        #aufgabe = p.read_text()
        aufgabe = my_read_text(p)

    if aufgabe:
        if not os.path.exists(get_aufgabe_dir(aufgabe)):
            # print("get_aktive_aufgabe -> '{0}' does not exist. Return None.".format(aufgabe))
            aufgabe = None

    return aufgabe

def is_aufgabe_solved(aufgabe):
    fd = read_status_file()
    return fd[aufgabe] == 'solved'

#def active_unsolved_aufgabe_or_next():
#    """
#    """
#    aktive_aufgabe = get_aktive_aufgabe()
#    if not aktive_aufgabe:
#        return None
#
#    if not is_aufgabe_solved(aktive_aufgabe):
#        return aktive_aufgabe
#    else:
#        return get_next_aufgabe()
#

def set_aktive_aufgabe(aufgabe):
    configdir = get_config_location()
    if not os.path.exists(configdir):
        Path(configdir).mkdir(parents=True)
    p = Path(os.path.join(get_config_location(), 'activetask'))

    # p.write_text(aufgabe) # TODO later
    my_write_text(p, aufgabe) # remove later

def aufgabe_by_name_or_number(name_or_number):
    """
    Wenn name_or_number keine Zahl, dann wird geschaut, ob es eine Aufgabe mit diesem Namen gibt.
        Wenn ja: print und zurückliefern
        Wenn nein: return None
    Ansonsten wird die Aufgabe per Zahl gesucht (siehe --print-status)
    und zurückgeliefert.
    """
    if not name_or_number:
        print("ASSERT FAILED: Argument darf nicht None sein")
        return None

    fd = read_status_file()

    if not name_or_number.isdigit():
        name = name_or_number
        if name in fd.keys():
            return name
        else:
            return None

    nr = 1
    for a in natural_sorted(fd.keys()):
        aufgabe = a
        if nr == int(name_or_number):
            return aufgabe
        nr = nr + 1
    return None

def get_first_aufgabe():
    return get_available_aufgaben()[0]

def get_next_aufgaben(aufgabe):
    """
    Gibt ausgehend von der gegebenen Aufgabe die (eine oder mehrere) nächste Aufgaben zurück.
    Wenn None übergeben wird, dann wird die erste aller Aufgaben gewählt.
    Liefert [], falls nichts gefunden.
    Hinweis: Es kann auch die aktive Aufgabe unter den nächsten sein. Das muss woanders gefiltert werden
    """

    if not aufgabe:
        return [ get_available_aufgaben()[0] ]

    children = get_direct_children(aufgabe)

    if len(children) == 1:
        return children

    if len(children) > 1:
        return children

    p = get_parent_with_atleast_2_children(aufgabe)

    return get_direct_children(p)

def get_next_aufgabe_interactive():
    """
    Gibt die nächste Aufgabe in Relation aktiven zurück.
    Wenn es mehrere gibt, wird ein Hinweis (mit weiteren Befehlen) ausgegeben und None zurückgegeben.

    Die nächste Aufgabe ist
    1. das nächste Kind im Baum
    2. bei mehr als einem Kind, interaktive Rückfrage
    3. bei keinem Kind, siehe get_next_aufgaben und interaktive Rückfrage, weil das dann mehr als eine Aufgabe ist
    4. die nächste im Alphabet
    5. None, wenn die letzte erreicht wurde bzw. der Algorithmus noch nichts besseres gefunden hat

    Fragt nach, wenn die aktuelle Aufgabe noch nicht gelöst ist
    """
    aktive_aufgabe = get_aktive_aufgabe()

    if aktive_aufgabe and not is_aufgabe_solved(aktive_aufgabe):
        print("")
        user_input = input("Die aktive Aufgabe ist noch nicht gelöst. Trotzdem fortfahren? (JA/NEIN) ").lower()
        if not user_input in [ "ja", "j" ]:
            return None

    #suggested_next = get_suggested_next_aufgabe(aktive_aufgabe)
    #if not suggested_next == None:
    #    a = aufgabe_by_name_or_number(suggested_next)
    #    if not a:
    #        print("ERROR: suggested_next file contains garbage")
    #        return None
    #
    #    return a

    next_a_list = get_next_aufgaben(aktive_aufgabe)
    if aktive_aufgabe and aktive_aufgabe in next_a_list: # die aktive Aufgabe kann auch mit drin sein; diese entfernen
        next_a_list.remove(aktive_aufgabe)

    # (1)
    if len(next_a_list) == 1:
        return next_a_list[0]

    # (2), (3)
    if len(next_a_list) > 1:
        # refresh_cache_aufg_nr_map() # see init_config_and_cache
        print("")
        print("Es gibt mehr als eine passende nächste Aufgabe.")
        print("Du kannst wählen wie es weitergehen soll.")
        print("")
        for a in next_a_list:
            solved_str = ""
            if is_aufgabe_solved(a):
                solved_str = " (\033[92mbereits gelöst\033[0m)"
            print("  --> Aufgabe Nr. {0}: '{1}'{2}".format(get_aufg_nr_cached(a), get_aufgabe_title(a), solved_str))
        print("")
        print("Starte die gewünschte Aufgabe mit einem dieser Befehle:")
        for a in next_a_list:
            print("    shelltux start {0}".format(get_aufg_nr_cached(a)))

        print("")
        return None

    aufgaben = get_available_aufgaben()
    next_i = aufgaben.index(aktive_aufgabe) + 1
    if next_i >= len(aufgaben): # reached last aufgabe
        # (5)
        return None

    # (4)
    return aufgaben[next_i]

def get_nextnew_aufgabe():
    """
    Gibt die nächste Aufgabe zurück, die noch nicht gelöst wurde.
    Oder None, wenn es ab der aktiven keine weitere aufgabe mehr gibt.
    """
    aktive_aufgabe = get_aktive_aufgabe()

    fd = read_status_file()

    # sortieren nach Aufgaben-Name; die Namen enthalten Nummern und legen die Reihenfolge fest
    aufgaben_liste = natural_sorted(fd.keys())

    # erste Aufgabe zurückgeben, wenn es noch keine aktive Aufgabe gibt
    if not aktive_aufgabe:
        return aufgaben_liste[0]

    aufgabe_aktiv_index = aufgaben_liste.index(aktive_aufgabe)

    for a in aufgaben_liste[aufgabe_aktiv_index:]:
        if fd[a] in [ 'new' ]:
            return a

    return None

def get_suggested_next_aufgabe(aufgabe): # DERZEIT NICHT GENUTZT
    """
    Gibt die Aufgabe in der Datei suggested_next zurück, oder None, falls nicht existiert.
    """
    suggested_next_file = os.path.join(get_aufgabe_dir(aufgabe), 'suggested_next')
    if os.path.exists(suggested_next_file):
        aufgabe = my_read_text(Path(suggested_next_file)).strip()
        return aufgabe

    return None

def get_dependson_aufgaben(aufgabe):
    """
    Gibt die Liste aller direkten Eltern-Aufgaben (aus der depends_on-Datei) zurück, die vorher gelöst werden müssen.
    Die Aufgaben können mit Leerzeichen oder Leerzeilen getrennt werden. Bevorzugt sind Leerzeilen.
    """
    if not aufgabe:
        return []

    depends_file = os.path.join(get_aufgabe_dir(aufgabe), 'depends_on')
    if os.path.exists(depends_file):
        depends_line = my_read_text(Path(depends_file)).strip()
        return depends_line.split() # If sep is not specified or is None, any whitespace string is a separator and empty strings are removed from the result

    return []

def _get_dependson_aufgaben_recurse_impl(result, aufgabe):
    l1 = get_dependson_aufgaben(aufgabe)

    if len(l1) > 0:
        for a in l1:
            if a not in result:
                result.add(a)
                s2 = _get_dependson_aufgaben_recurse_impl(result, a)
                result = result.union(s2)

    return result

def get_dependson_aufgaben_recurse(aufgabe):
    """
    Gibt die Liste aller (Eltern-)Aufgaben (rekursiv) zurück, die vorher gelöst werden müssen
    """
    if not aufgabe:
        return []

    # create set https://docs.python.org/3/tutorial/datastructures.html
    result = _get_dependson_aufgaben_recurse_impl(set(), aufgabe)
    return list(result)

def get_direct_children(aufgabe):
    """
    """
    children_map = { } # Map: aufgabe -> list of children

    aufgaben = get_available_aufgaben()

    for a in aufgaben:
        children_map[a] = set() # prepare for nodes without children
        parents = get_dependson_aufgaben(a)
        for p in parents:
            if not p in children_map:
                children_map[p] = set()
            children_map[p].add(a)

    return natural_sorted(list(children_map[aufgabe])) # sorted to have a stable order

def get_parent_with_atleast_2_children(aufgabe):
    """
    The parent or None if not found.
    TODO: This does not take all cases into account but it is a start; think about a better traversing strategy
    """
    parents = get_dependson_aufgaben(aufgabe)

    if len(parents) == 0:
        return None

    for p in parents:
        children = get_direct_children(p)
        if len(children) >= 2:
            return p

    # Now we always choose the first parent (if there are several) for recursion; better: traverse everything
    return get_parent_with_atleast_2_children(parents[0])

def is_bonus(aufgabe):
    """
    Gibt True zurück, wenn die Aufgabe eine Bonus-Aufgabe ist
    """
    bonus_file = os.path.join(get_aufgabe_dir(aufgabe), 'bonus')
    return os.path.exists(bonus_file)

def print_status(all_tasks):
    """
    Gibt die Liste aller Aufgaben aus.
    Mit all_tasks False: nur die schon gelösten und die aktive (und alle die, die f
ür die aktive Aufgabe benötigt werden, "depends_on")
    Mit all_tasks True: alle; außerdem noch die ID-Spalte
    """
    fd = read_status_file()
    activetask = get_aktive_aufgabe()
    all_dependson_activetask = get_dependson_aufgaben_recurse(activetask)

    idcol_title = "| ID"
    idcol_stroke = "`------------------"
    if not all_tasks:
        idcol_title = ""
        idcol_stroke = ""
    print("{0:<7}{1:<28}{2:<13}{3:<10}{4:<20}".format("Nr.", "Aufgabe", "Status", "Aktiv", idcol_title))
    print("{0:<7}{1:<28}{2:<13}{3:<10}{4:<20}".format("----", "-------------------------", "----------", "-----", idcol_stroke))
    nr = 1
    for a in natural_sorted(fd.keys()):
        aufgabe = a

        # Skip these when all_tasks = False.
        # Skip when
        # es ist nicht die aktuelle Aufgabe UND Aufbabe ist offen "offen" UND die aktuelle Aufgabe hängt nicht davon a
        if not all_tasks and not aufgabe == activetask and (fd[a] == 'new' and not aufgabe in all_dependson_activetask):
            nr = nr + 1
            continue

        status = "unbekannt"
        aktiv = ""
        offen = fd[a] == 'new'
        solved = fd[a] == 'solved'
        bonus = is_bonus(aufgabe)
        titel = get_aufgabe_title(aufgabe)

        if offen:
            status = "offen"
        #elif fd[a] == 'active':      # REMOVE
            #status = "offen/aktiv"
        elif solved:
            status = "gelöst :-)"
        if aufgabe == activetask:
            aktiv = "AKTIV"
        else:
            if aufgabe in all_dependson_activetask:
                if solved:
                    aktiv = "N" # N wie nötig
                else:
                    # aktiv = "\033[1;91mN!\033[0m" messes up with format indentation
                    aktiv = "N!"

        if bonus:
            titel = "* " + titel

        idcol_text = "  " + aufgabe
        if not all_tasks:
            idcol_text = ""

        print("{0:<7}{1:<28}{2:<13}{3:<10}{4:<20}".format(nr, titel, status, aktiv, idcol_text))

        nr = nr + 1

    print("")
    print("Spalte Nr          : ausgewählte Aufgabe starten: shelltux start <Nr>")
    print("Spalte Aufgabe: *  : Bonus-Aufgabe")
    print("Spalte Aktiv:   N  : die Aufgabe ist notwendig für die aktuelle Aufgabe")
    print("                N! : wie N, aber noch nicht gelöst")
    print("")

    # TODO: falls bestimmte Aufgaben bestimmte Voraussetzungen haben (wie z. B. cowsay o. ä.), dann sollte das angezeigt werden, wenn fehlt

def get_merkblatt_for_solved(aufgabe):
    """
    Return the path to the merkblatt-on-solved file if exists. Else None
    """
    merkblatt = os.path.join(get_aufgabe_dir(aufgabe), 'merkblatt-on-solved')
    if os.path.exists(merkblatt):
        return merkblatt

    return None

def aktive_aufgabe_set_solved():
    fd = read_status_file()

    aufgabe = get_aktive_aufgabe()

    if not aufgabe:
        print("Keine aktive Aufgabe. Keine Aktion.")
        print("")
        return

    fd[aufgabe] = 'solved'
    write_status_file(fd)

def print_merkblatt_formatted(merkblatt):
    """
    merkblatt: path to the merkblatt

    (Deutlich langsamer als einfach nur print)

    Hinweise:
        - troff formatting, siehe https://liw.fi/manpages/
        - siehe Datei mancat
        - siehe Datei merkblatt-format

    BUGS/todo:
        - mit less verschwindet die Formatierung und die mit ./mancat weggemachten Zeilen tauchen wieder auf!

    Weiteres, noch nicht näher untersucht:
        - https://unix.stackexchange.com/questions/34516/can-i-create-a-man-page-for-a-script
            - https://txt2tags.org/
    """

    #print(Path(merkblatt).read_text())
    # print(my_read_text(Path(merkblatt))) # old

    subprocess.call([ os.path.join(get_lib_dir(), 'merkblatt-format'), os.path.join(get_lib_dir(), 'mancat'), merkblatt ])

def aktive_aufgabe_query_solution():
    """
    todo: doc
    """
    aufgabe = get_aktive_aufgabe()

    if not aufgabe:
        print("Keine aktive Aufgabe. Keine Aktion.")
        return

    print("")

    # Es gibt entweder eine feste Lösung in der Datei solution.
    # Oder ein Testscript in der Datei solution-test (als ersten Parameter wird ST_CURRENTWORKDIR übergeben).

    solution_fixed = get_solution_fixed(aufgabe)
    solution_ack = get_solution_simple_ack(aufgabe)
    solution_test_script = get_solution_test_script(aufgabe)
    solution_later = get_solution_later(aufgabe)

    # todo: check that only one of them is given

    is_ok = False

    if solution_fixed:
        user_input = input("Hier die Lösung eingeben und dann Enter drücken: ".format(aufgabe))
        is_ok = user_input.strip() == solution_fixed.strip()
    elif solution_ack:
        user_input = input(solution_ack.strip() + " (JA / NEIN) ") # z. B. "Alles klar?"
        is_ok = user_input.strip().lower() == "ja"
    elif solution_later:
        user_input = input("Bitte frage deinen Lehrer(m/w), ob die Lösung richtig ist. ... ... (JA / NEIN) ")
        is_ok = user_input.strip().lower() == "ja"
    elif solution_test_script:
        is_ok = run_solution_test(aufgabe)
    else:
        print("ERROR: Keine SOLL-Lösung vorhanden. Siehe def aktive_aufgabe_query_solution für Details.")

    print("")

    def _print_merkblatt():
        merkblatt = get_merkblatt_for_solved(aufgabe)
        if merkblatt:
            print("")
            print("Für dein MERKBLATT Folgendes notieren (oder merken):")
            print("-------------------------------------------------------------------")
            print("")
            print_merkblatt_formatted(merkblatt)
            print("")
            print("-------------------------------------------------------------------")

    if solution_fixed or solution_test_script:
        if is_ok:
            print("    :-D   \033[92mDie Lösung ist RICHTIG.\033[0m   :-D")
            _print_merkblatt()
            print("")
            print("Weiter zur nächsten Aufgabe mit")
            print("")
            print("    shelltux next")
            print("")
            aktive_aufgabe_set_solved() # todo: use specific aufgabe instead of aktiv
        else:
            print("    Die Lösung ist \033[91mnoch nicht vollständig oder falsch.\033[0m   :-(")
            print("")
            print("Was nun?")
            print("    1. Vervollständige die Aufgabe oder suche den Fehler.")
            print("    2. Beginne die Aufgabe von vorne mit: shelltux start")
            print("    3. Frage den Lehrer(m/w), wenn du nicht weiterkommst.") # als erstes gemeinsam durch die Aufgabe gehen und schauen, woran es liegt (Fehler in der Aufgabe?)
            print("")
            # TODO: Aufgabe auf Nachfrage wieder als ungelöst markieren
    elif solution_ack:
        if is_ok:
            print("JA: Sehr schön. Die Aufgabe wurde als gelöst markiert.")
            _print_merkblatt()
            print("")
            print("Weiter mit shelltux next")
            aktive_aufgabe_set_solved() # todo: use specific aufgabe instead of aktiv
        else:
            print("NEIN: --> Frage deinen Lehrer(m/w), was du als nächstes machen könntest.")
            # todo: Aufgabe ggf. wieder ungelöst markieren
        print("")
    elif solution_later:
        if is_ok:
            print("JA: Sehr schön. Die Aufgabe wurde als gelöst markiert.")
            _print_merkblatt()
            print("")
            print("Weiter mit shelltux next")
            aktive_aufgabe_set_solved() # todo: use specific aufgabe instead of aktiv
        else:
            print("NEIN: --> Frage deinen Lehrer(m/w), was du als nächstes zu tun ist.")
            # todo: Aufgabe ggf. wieder ungelöst markieren
        print("")

def print_merkblatt(all):
    """
    TODO: bei all=False, auch die Merkblätter der zwar noch nicht gelösten, aber abhängigen Aufgaben ausgeben
    """
    #print("Das MERKBLATT")
    #print("=============")
    if all:
        print("Das Merkblatt für alle Aufgaben, inklusive Aufgaben-Titel.")
    else:
        print("Das Merkblatt für alle bereits gelösten Aufgaben, ohne Aufgaben-Titel.")

    for aufgabe in get_available_aufgaben():
        if not all and not is_aufgabe_solved(aufgabe):
            continue

        merkblattpath = os.path.join(get_aufgabe_dir(aufgabe), 'merkblatt-on-solved')

        if all:
            print("")
            aufgabetitlestr = "Aufgabe: {0} ({1})".format(get_aufgabe_title(aufgabe), aufgabe)
            print(aufgabetitlestr)
            print("-" * len(aufgabetitlestr))

        if os.path.exists(merkblattpath):
            print_merkblatt_formatted(merkblattpath)
        else:
            if all: # nur wenn wir alle Aufgaben anzeigen, wollen wir wissen, ob das Merkblatt leer ist
                print("(nichts)")

        print()

def _graph_node_text(aufgabe, nr):
    s = "{0}\\n({1})".format(get_aufgabe_title(aufgabe), aufgabe)
    if nr:
        s += "\\n{0}".format(nr)
    if is_bonus(aufgabe):
        s = "Bonus:\\n" + s
    return s

def get_aufg_nr_cached(aufgabe):
    """
    Liefert die Nummer zur Aufgabe. Oder None, wenn nicht gefunden
    Erfordert refresh_cache_aufg_nr_map().
    """
    if aufgabe in cache_aufg_nr_map:
        return cache_aufg_nr_map[aufgabe]
    return None

def refresh_cache_aufg_nr_map():
    """
    Damit get_aufg_nr_cached funktioniert.
    """
    cache_aufg_nr_map.clear();
    nr = 1
    for a in get_available_aufgaben():
        cache_aufg_nr_map[a] = nr
        nr += 1

def show_dep_graph():
    """
    Outputs a graph definition to stdout.

    Use this like this:

    $ ... > g1.gv
    $ dot -Tps g1.gv -o g1.ps
    $ xdg-open g1.ps

    In short:
    $ ./stutil.py --show-dep-graph > /tmp/g1.gv && dot -Tps /tmp/g1.gv -o /tmp/g1.ps && xdg-open /tmp/g1.ps
    """

    # refresh_cache_aufg_nr_map() # see init_config_and_cache

    activetask = get_aktive_aufgabe()
    all_dependson_activetask = get_dependson_aufgaben_recurse(activetask)

    # see https://graphviz.gitlab.io/_pages/pdf/dotguide.pdf, page 3 etc.
    # see also http://graphs.grevian.org/
    # see also https://stackoverflow.com/questions/9106079/graphviz-how-to-change-border-color
    digraph = "digraph G {\n"
    for a in get_available_aufgaben(): # natural_sorted
        da_list = get_dependson_aufgaben(a)
        solved = is_aufgabe_solved(a)
        a_nodename = a.replace('-', '_').replace('.', '_')

        # default style
        digraph = digraph + '{0} [color="#000000",fillcolor="#FFFFFF",peripheries=1];\n'.format(a_nodename)
        # "Gelöst" = GRÜN
        if solved:
            digraph = digraph + '{0} [color="#000000",style=filled,fillcolor="#00CC00",peripheries=1];\n'.format(a_nodename)
        # "Noch nicht gelöst, aber als nächstes möglich" = GELB
        # https://stackoverflow.com/questions/10666163/how-to-check-if-all-elements-of-a-list-matches-a-condition
        if not solved and all(get_aufg_nr_cached(a1) and is_aufgabe_solved(a1) for a1 in get_dependson_aufgaben(a)):
            digraph = digraph + '{0} [color="#000000",style=filled,fillcolor="#FFFF00",peripheries=1];\n'.format(a_nodename)
        # "Für die aktive Aufgabe nötig, aber nicht gelöst" = ORANGE und 2 Ränder
        if a in all_dependson_activetask and not solved:
            digraph = digraph + '{0} [color="#000000",style=filled,fillcolor="#FF8000",peripheries=2];\n'.format(a_nodename)
        # "Aktive Aufgabe" = drei Ränder
        if activetask == a:
            digraph = digraph + '{0} [color="#000000",peripheries=3];\n'.format(a_nodename)

        # labels
        digraph = digraph + '{0} [label="{1}"];\n'.format(a_nodename, _graph_node_text(a, get_aufg_nr_cached(a)))

        for a2 in da_list:
            a2_nodename = a2.replace('-', '_').replace('.', '_')
            # we reverse the dependency arrow to have a1.1-intro listed on top
            digraph = digraph + '   "{0}" -> "{1}";\n'.format(a2_nodename, a_nodename)

        if len(da_list) == 0 and not a == "a1.1-intro":
            digraph = digraph + '   "(noch keine Abhängigkeit definiert)" -> "{0}\\n({1})";\n'.format(get_aufgabe_title(a), a)

    digraph = digraph + "}"

    print(digraph)

def main():
    # https://docs.python.org/3.3/library/argparse.html
    # https://stackoverflow.com/questions/32270786/python-argparse-strict-variable-name    python >= 3.5
    parser = argparse.ArgumentParser(description='shelltux-util') #, allow_abbrev=False) # TODO: bring back in later!
    parser.add_argument('--first-aufgabe', action='store_true',
                    help='Gibt den Namen der ersten Aufgabe zurück')
    parser.add_argument('--nextnew-aufgabe', action='store_true',
                    help='Gibt den Namen der nächsten neuen (ungelösten) Aufgabe zurück. Nichts, wenn keine vorhanden.')
    parser.add_argument('--next-aufgabe-interactive', action='store_true',
                    help='Gibt den Namen der nächsten Aufgabe in einer Datei zurück: /tmp/st_<PID>_aufgabe". Bei mehreren Möglichkeiten wird nachgefragt. Die Datei ist leer, wenn keine Aufgabe gestartet werden soll.')
    parser.add_argument('--aktive-aufgabe', action='store_true',
                    help='Gibt den Namen der aktiven Aufgabe zurück oder nichts wenn es keine aktive Aufgabe gibt')
    #parser.add_argument('--active-unsolved-aufgabe-or-next', action='store_true',
    #                help='Gibt den Namen der aktiven Aufgabe zurück, falls diese noch nicht gelöst ist oder die nächste Aufgabe. Oder nichts, falls es keine aktive Aufgabe gibt')
    parser.add_argument('--set-aktive-aufgabe',
                    help='Setzt die aktive Aufgabe: <Name der Aufgabe>')
    parser.add_argument('--aufgabe-by-name-or-number',
                    help='Gibt den Namen der Aufgabe aus: <Name oder Nummer der Aufgabe>. Nummer siehe --print-status')
    parser.add_argument('--print-status', action='store_true',
                    help='Listet die Aufgaben auf, die schon gelöst wurden und die aktive')
    parser.add_argument('--all', action='store_true',
                    help='Für --print-status: listet alle die Aufgaben auf, auch die noch nicht gelöst wurden')
    parser.add_argument('--aktive-aufgabe-set-solved', action='store_true',
                    help='Setzt die aktive Aufgabe auf gelöst und markiert die nächste Aufgabe als aktiv')
    parser.add_argument('--aktive-aufgabe-query-solution', action='store_true',
                    help='Fragt nach der Lösung der aktuellen Aufgabe')
    parser.add_argument('--print-merkblatt-current', action='store_true',
                    help='Gibt das Merkblatt aller gelösten Aufgaben aus')
    parser.add_argument('--print-merkblatt-all', action='store_true',
                    help='Gibt das komplette Merkblatt aus')
    parser.add_argument('--show-dep-graph', action='store_true',
                    help='Zeigt den Abhängigkeitsgraph der Aufgaben')

    args = parser.parse_args()
    # print("shelltux-util")
    # print("Args:")
    # print(args)

    # see inside
    init_config_and_cache()

    if args.first_aufgabe:
        print(get_first_aufgabe())
        return

    if args.nextnew_aufgabe:
        a = get_nextnew_aufgabe()
        if a:
            print(a)
        return

    if args.next_aufgabe_interactive:
        a = get_next_aufgabe_interactive()
        with open('/tmp/st_{0}_aufgabe'.format(os.getpid()), 'w') as f:
            if not a:
                f.write('')
            else:
                f.write(a)
        return

    if args.aktive_aufgabe:
        a = get_aktive_aufgabe()
        if a:
            print(a)
        return

    #if args.active_unsolved_aufgabe_or_next:
    #    a = active_unsolved_aufgabe_or_next()
    #    if a:
    #        print(a)
    #    return

    if args.set_aktive_aufgabe:
        set_aktive_aufgabe(args.set_aktive_aufgabe)
        return

    if args.aufgabe_by_name_or_number:
        a = aufgabe_by_name_or_number(args.aufgabe_by_name_or_number)
        if a:
            print(a)
        return

    if args.print_status:
        print_status(args.all)
        return

    if args.aktive_aufgabe_set_solved:
        aktive_aufgabe_set_solved()
        return

    if args.aktive_aufgabe_query_solution:
        aktive_aufgabe_query_solution()
        return

    if args.print_merkblatt_current:
        print_merkblatt(all=False)
        return

    if args.print_merkblatt_all:
        print_merkblatt(all=True)
        return

    if args.show_dep_graph:
        show_dep_graph()
        return

if __name__ == "__main__":
    # execute only if run as a script
    main()
