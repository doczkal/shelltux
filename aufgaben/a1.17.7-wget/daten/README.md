Bilder herunterladen
====================

Auf der Wikipedia-Seite https://de.wikipedia.org/wiki/Tux_(Maskottchen)
ist unter dem schwarz-weiß-orangenen Tux-Pinguin das Bild eines
grau-weißen Zwergpinguins zu finden.

Aufgabe 1
---------
Wenn man auf das Vorschau-Bild des grau-weißen Zwergpinguins klickt,
dann öffnet sich eine große Version des Bildes.

Lade mit wget nun zwei Dateien herunter:

1. das Vorschau-Bild des Zwergpinguins
2. die große Version des Zwergpinguin-Bildes

Hinweise:
a) Die URL des Vorschaubildes bekommst du, indem du mit der rechten
Maustaste auf das Bild klickst und dort "Linkadresse kopieren" auswählst.
b) Du kannst die Adresse (URL) in der Zwischenablage mit
Strg+Umschalt+V in die Konsole einfügen
c) Beim großen Bild: klicke es so an, dass nur noch das Bild ohne
den Wikipedia-Bildbetrachter im Browser zu sehen ist. Dann kopiere die
Adresse aus der Adresszeile des Browsers.
d) Wenn die Adresse Leerzeichen oder Klammern enthält, dann verwende
anstelle von `wget URL`, die Form `wget "URL"` (also mit Anführungs-
zeichen um die URL).

Prüfe, ob die beiden Bilder angekommen sind.

Aufgabe 2
---------
Erstelle einen neuen Ordner "ergebnis" und verschiebe die beiden
Bilddateien dort hinein.

Hinweis: Erinnere dich an die Befehle mkdir und mv.

Aufgabe 3
---------
Wechsle in den Ordner "ergebnis" (cd) und liste die Dateien
mit ls -l auf.

Bei der Ausgabe von `ls -l` steht in der fünften Spalte
die Dateigröße. So sieht das z. B. aus:


        1      2   3      4     5   6            7
                                |
                                v
    -rw-r--r-- 1 gregor users   43 Apr 13 02:20 dok1
    -rw-r--r-- 1 gregor users 2710 Apr 13 02:21 dok2
    -rw-r--r-- 1 gregor users    2 Dez 17 23:15 dok3

Finde die Dateigröße der beiden Bilder heraus.
Addiere die beiden Zahlen und schreibe das Ergebnis in eine
Datei mit Namen 'g'. Dann weiter mit shelltux solve.

Hinweis: Zum Erstellen der Datei g erinnere dich an den
      echo-Befehl mit Umleitung in eine Datei,
      z. B. echo "hallo" > datei

