Nadelbaum
=========
Im Unterverzeichnis `baum` ist eine Datei zu finden.
Der Dateiname beginnt mit fünf Leerzeichen und dann ein B:

Deine Aufgabe ist nun mit touch Dateien so zu erzeugen,
dass ein Nadelbaum entsteht, wenn man die Dateien im Verzeichnis
`baum` mit

    ls -1

auflistet. (Das Zeichen in "-1" ist eine Eins und KEIN kleines L)

1. Schaue dir vorher die Datei `ergebnis` an. So soll das
    Bild am Ende aussehen.
2. Wechsele ins Verzeichnis `baum`, erzeuge dort Dateien
    und prüfe mit `ls -1` das Zwischenergebnis
3. `shelltux solve`, wenn du fertig bist.

TIPP: Die nächste Datei für die Baum-Krone wird so erzeugt:

    touch "    BBB"

Die Anführungszeichen braucht man wegen der vier Leerzeichen
am Anfang.
