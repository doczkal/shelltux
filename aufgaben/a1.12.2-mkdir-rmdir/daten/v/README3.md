Aufgaben zu Verzeichnissen (3)
==============================

Aufgabe 4
---------
Mit dem Befehl

    rm -R VERZEICHNIS

kann man ein Verzeichnis löschen, auch wenn es noch nicht leer ist.
Dabei wird der gesamte Inhalt gelöscht.

Das "-R" steht dabei für "rekursiv". Den Begriff lernen wir später
genauer kennen.

VORSICHT: mit `rm -R` Befehl kannst du ganz schnell und einfach
eine große Menge an Dateien löschen. Also genau aufpassen.

Lösche nun das Verzeichnis `qq`.

Dann weiter mit README4.md...
