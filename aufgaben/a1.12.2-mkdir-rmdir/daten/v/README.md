Aufgaben zu Verzeichnissen (1)
==============================

Aufgabe 1
---------
a) Nutze den Befehl `mkdir`, um im aktuellen Verzeichnis das
Verzeichnis mit Namen `a` zu erzeugen.
b) Wechsele in das neue Verzeichnis und erzeuge dort eine
neue leere DATEI mit Namen `d`.

Aufgabe 2
---------
Gehe zurück ins Verzeichnis `v` und erzeuge dort ein
neues Verzeichnis `b`. Wechsle hinein und erzeuge dort
noch ein Verzeichnis `c`.

Weiter mit README2.md...
