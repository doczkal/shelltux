Aufgaben zu Verzeichnissen (4)
==============================

Aufgabe 5
---------
Bei vielen Befehlen gibt es die Option "-v".

Das steht für verbose (engl. für wortreich oder langatmig).

Bei rm zeigt diese Option Details zum Löschvorgang an.
Hier zum Beispiel die rekursive Löschung mit Ausgabe von Details:

    rm -Rv VERZEICHNIS

Lösche damit das Verzeichnis `www`. In der Ausgabe ist eine kleine
Rechenaufgabe versteckt.
Merke dir die Zahl für die Lösung gib `shelltux solve` ein.
