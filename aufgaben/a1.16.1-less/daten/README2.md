README2
=======

AUFGABE
-------

Überfliege die Dateien im Verzeichnis 'tech-info'
und halte Ausschau nach einer ASCII-Art-Figur.
In jeder Datei ist eine Figur enthalten und die sagt etwas
mit Hilfe ihrer Sprechblase. Das, was gesagt wird,
trägt zur Lösung bei.

Es sind vier Dateien zu durchsuchen. Dann shelltux solve
