echo und Umleitung in Dateien
=============================

Man kann echo dafür verwenden, um Text in Dateien zu schreiben:

    echo TEXT > DATEI

Mit
    echo "Hallo Welt" > welt

erzeugst du eine neue Datei `welt`, die den Text `Hallo Welt` enthält.
Probiere es aus und schaue mit dem cat-Befehl, ob das Ergebnis stimmt.

Aufgabe
-------

In der Umgebungsvariable HOSTNAME steht der Name des Rechners, auf dem
du eingeloggt bist.

Erzeuge eine neue Datei `rechner`, die den Namen des Rechners enthält.

Weiter mit `shelltux solve`.

