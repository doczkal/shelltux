echo und Umgebungsvariablen
===========================

In deiner Bash-Shell sind standardmäßig eine Reihe von
Umgebungsvariablen vorhanden.

Wenn du interessiert bist, dann gib

    env

ein. Es kommt eine lange Ausgabe, die du jetzt noch nicht verstehen musst.

Unter den Variablen gibt es eine, die USER heißt. Die enthält
deinen Login-Namen.

Gib den Befehl ein:

    echo $USER

Das Dollarzeichen markiert den Beginn des Variablennamens.

Probiere das hier aus:

    echo "Hallo, ich bin hier als $USER eingeloggt."

Weiter mit README2.md

