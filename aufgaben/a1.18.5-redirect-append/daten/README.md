Werkzeuge
=========

Die Geschichte soll in die Datei g geschrieben werden.

Gib zur Übung folgende Befehle ein:

    echo Meine Überschrift > g
    cat g

Die Datei sollte den Text "Meine Überschrift" enthalten.

Jetzt

    cowsay Hallo

Du siehst nun die Ausgabe des cowsay-Programms.

Jetzt an die Datei anhängen:

    cowsay Hallo >> g
    cat g

WICHTIG ist der Doppelpfeil (>>) in der zweiten Zeile. Denn
er bedeutet "Anhängen". Wenn du einen Einzelpfeil (>) verwendet hättest,
wäre die Überschrift weg. Also aufpassen.

Das hier kannst du auch für die Geschichte verwenden:

    figlet Grosser Text
    cowsay -f bunny "Mein Name ist Hase"

Probiere es aus.

So, jetzt kannst du loslegen mit deiner Geschichte. Mit less kannst du
dir zwischendurch anschauen, ob alles gut aussieht.

Wenn du die Geschichte behalten willst, dann kopiere sie in dein
HOME-verzeichnis (cp g ~).
