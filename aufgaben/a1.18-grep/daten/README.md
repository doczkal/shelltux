grep
====

Suche in der Datei 'wiki-artikel-1' nach dem Wort "Mr. X"
und finde dann heraus, in welcher Zeile er sich befindet.

Die Zeilennummer für die Lösung merken.

Tipp:

    1. Mit cat -n DATEI kannst du dir Zeilennummern ausgeben lassen.
    2. Die gesuchte Zeile ist zwischen der 50. und der 90. zu finden.

Die nächste Aufgabe befindet sich in der README-2.md.

