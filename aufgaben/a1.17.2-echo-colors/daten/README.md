Farben
======

Einleitung
----------

Gib folgendes ein:

    echo -e "\e[32mgrün\e[0m"

Der Text sollte grün sein.

Das funktioniert mit sogenannten "Escape Sequenzen".
Sie müssen über die Option -e aktiviert werden.

Allgemein gilt:

\e[CODEm   - Formatierung mit CODE starten, 32 steht für grün
\e[0m      - Formatierung beenden (0 steht für Beenden)

Ein längeres Beispiel:

    echo -e "Hallo \e[32mgrüne\e[0m und \e[31mrote\e[0m Welt.\e[0m"

Der Code 31 steht für rot.

Rufe
    ./show-bash-formatting.sh | less

auf, um weitere Codes zu sehen.

Aufgabe
-------
Gib mit echo folgendes auf der Konsole aus:

    Hallo, mein Name ist <Vorname> <Nachname> und mein Benutzername ist <$USER>.

wobei folgendes gilt:

<Vorname>  = dein Vorname in hellmagenta Schriftfarbe
<Nachname> = dein Nachname mit blauem Hintergrund
<$USER>    = der Wert der Variablen USER unterstrichen

Danach weiter mit shelltux solve.
