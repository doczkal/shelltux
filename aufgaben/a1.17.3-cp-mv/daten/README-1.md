cp-Aufgaben
===========

Aufgabe 1
---------
Im Verzeichnis a1 befindet sich eine Datei artikel.txt.

Nutze die Variante

    cp DATEI_1 DATEI_2

von cp, um die Datei artikel.txt in eine neue Datei mit Namen
artikel-kopie.txt zu kopieren.
Prüfe mit ls, ob die neue Datei wirklich da ist.

Aufgabe 2
---------
Erstelle parallel zum Verzeichnis a1 ein neues Verzeichnis a2.

Verwende die Variante

    cp DATEI VERZEICHNIS

um die Datei artikel.txt in das Verzeichnis a2 zu kopieren.
Prüfe nach, ob die neue Datei wirklich da ist.

Aufgabe 3
---------
Man kann auch mehrere Dateien auf einmal kopieren:

    cp DATEI_1 DATEI_2 ... VERZEICHNIS

oder:

    cp DATEI_MIT_PLATZHALTER VERZEICHNIS

Erstelle ein Verzeichnis a3neu und kopiere alle Dateien aus
dem Verzeichnis a3 dort hinein (es sind neun Stück).
Prüfe nach, ob die neuen Datei im Zielverzeichnis liegen.

Gib shelltux solve ein, um den ersten Teil zu prüfen
und dann weiter mit README-2.md
