mv-Aufgaben
===========

Aufgabe 4
---------
Verschiebe alle neun Dateien aus dem Verzeichnis a4a in das
Verzeichnis a4b. Verwende dazu den mv-Befehl.

    mv DATEI VERZEICHNIS

Aufgabe 5
---------
Man kann mit mv nicht nur Verschieben, sondern auch umbenennen.

    mv DATEI_ALT DATEI_NEU

a) Kopiere die Datei README-2.md in das Verzeichnis a5
b) Benenne die Datei im Verzeichnis a5 um in Hallo.txt
c) Benenne dann das Verzeichnis a5 um in a5neu.

Weiter mit shelltux solve.
