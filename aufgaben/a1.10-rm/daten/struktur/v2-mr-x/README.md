Aufgabe 2
=========

Auch hier sollen alle Dateien gelöscht werden.

Es sind ziemlich viele Dateien. Zum Glück gibt es Platzhalter.

Ein Stern (*) steht für alle Dateien:

    rm *

Wenn du fertig bist, weiter mit shelltux solve.
