youtube-dl
==========

Mit youtube-dl kann man - wie man am Namen schon fast selber rausbekommen
hätte ;-) - auch YouTube-Videos auf den eigenen Rechner laden.

Mit der Option -x wird der Audio-Teil des Videos extrahiert und gespeichert.

HINWEIS: Bitte achte darauf, welche Lizenz das heruntergeladene Material hat.
Im Zweifel ist alles nur für den privaten Gebrauch zu verwenden und nicht
weiterzuverbreiten.
