Eigenes Script (1)
==================

nano
----
Zuerst lernen wir, wie man mit dem Texteditor nano Dateien bearbeitet.

Mache eine neue Shell auf, damit du die README.md offen behalten kannst,
während du in der zweiten Shell die Aufgabe löst.

Starte nano

    nano

Folgendes Bild erscheint

  GNU nano 2.9.5                     New Buffer



^G Get Help    ^O Write Out   ^W Where Is    ^K Cut Text    ^J Justify
^X Exit        ^R Read File   ^\ Replace     ^U Uncut Text  ^T To Spell


Schreibe einen kurzen Text, wie z. B. "Hallo Welt".

Drücke Strg+O - das Dach (^) steht für Strg.

Es erscheint die Meldung

    File Name to Write:

Gib einen Dateinamen ein. Z. B. "meinedatei". Drück dann die Enter-Taste.

Danach drücke Strg+X zum Verlassen von nano.

Schaue nun, ob du deine Datei findest, und ob dein Text drin steht,
mit ls und cat oder nano.

Weiter mit README2.md.
