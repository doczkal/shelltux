Eigenes Script (2)
==================

Bash-Script
-----------
1. Liste dir mit `ls aaa` den Inhalt des Verzeichnisse aaa auf
   und merke dir, welche Datei drin ist.

2. Schaue dir die Datei script1.sh mit less an. Die Befehle dort werden
   gleich der Reihe nach ausgefürt. Überlege vorher, was wohl
   auf der Konsole ausgegeben wird.

3. Führe das Script script1.sh aus, indem du folgendes eingibst:

    ./script1.sh

4. Hat die Ausgabe mit deinen Überlegungen übereingestimmt?
   Gehe außerdem ins Verzeichnis aaa und schaue, wie es jetzt aussieht.

Weiter mit README3.md
