Eigenes Script (3)
==================

Deine Aufgabe ist es nun, ein eigenes Script zu schreiben,
das folgendes macht:

1. Einen Begrüßungstext ausgeben, mit echo
2. Aus dem Verzeichnis bbb alle Dateien mit der Endung ".txt" ins
   Verzeichnis ccc kopieren.
3. Einen Verabschiedungstext ausgeben.

Beginne, indem du die Datei script1.sh in eine neue Datei script2.sh
kopierst und in dieser Datei alles bis auf die erste Zeile löschst.

Dann schreibe deinen Script-Code mit nano, speichere die Datei
und führe sie aus.

Weiter mit shelltux solve.
