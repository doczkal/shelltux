Prozesse auflisten
==================

Mit dem Befehl

    ps -axu

kannst du dir alle Prozesse auflisten, die auf dem System laufen.

Es gibt dabei folgende Spalten:

USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND

Davon sind für uns folgende interessant:

USER:       Der Benutzer, der den Prozess gestartet hat
PID:        Die Prozess-ID. Diese benötigt man z. B., um den Prozess zu beenden
START:      Uhrzeit, wann der Prozess gestartet wurde
COMMAND:    Wenn vorhanden, der Pfad/Dateiname des Programms und dessen Argumente

Probiere den Befehl aus und lies dann die README2.md.
